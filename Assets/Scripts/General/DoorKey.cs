﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorKey : MonoBehaviour
{
    [SerializeField] private Door[] doorsOpenedWithKey;
    [SerializeField] private Dialogue dialogue;

    private TextEventsManager textManager;

    private void Awake() => textManager = FindObjectOfType<TextEventsManager>();

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            foreach (Door door in doorsOpenedWithKey)
            {
                door.KeyTaken = true;
            }
            textManager.ProcessDialogue(dialogue, true, false);
            Destroy(gameObject);
        }
    }
}
