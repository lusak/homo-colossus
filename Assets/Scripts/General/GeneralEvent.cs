﻿using System;
using UnityEngine;

public class GeneralEvent : MonoBehaviour
{
    [SerializeField] private MonoBehaviour objectToCall;
    [SerializeField] private string methodToCall;

    private RegistrableObject registrable;

    private void Awake()
    {
        registrable = GetComponent<RegistrableObject>();

        if (methodToCall == null || String.IsNullOrEmpty(methodToCall))
        {
            Debug.LogError("GeneralEvent object with name '" + gameObject.name + "' does not have its mandatory fields assigned");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (registrable == null)
        {
            Debug.LogError("Missing registrable script");
            return;
        }

        if (collision.gameObject.tag == "Player")
        {
            if (registrable.OneTimeEvent && registrable.AlreadyVisited())
            {
                return;
            }

            if (!registrable.AlreadyVisited())
            {
                registrable.MarkAsVisited();
            }

            objectToCall.Invoke(methodToCall, 0f);

        }
    }
}

