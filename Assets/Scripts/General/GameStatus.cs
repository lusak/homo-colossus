﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStatus : MonoBehaviour
{
    [SerializeField] private static int deathReloadTime = 3;

    public static GameStatus instance;
    public Vector3 LastCheckpoint { get; set; }
    public string LastCameraConfinerColliderName { get; set; }
    private List<Vector3> visitedObjects = new List<Vector3>();
    private TransitionAnimations transition;

    private int menuSceneIndex = 1;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        } 
        else if (this != instance)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public void registerVisitedObject(Vector3 objectPosition) => visitedObjects.Add(objectPosition);

    public bool checkIfObjectVisited(Vector3 objectPosition)
    {
        if (visitedObjects.Contains(objectPosition))
        {
            return true;
        } else
        {
            return false;
        }
    }

    public void ReloadScene(int timeBeforeReload)
    {
        instance.StartCoroutine(instance.WaitAndRealoadScene(timeBeforeReload));
    }


    public void LoadNextScene()
    {
        ResetAllFields();
        Debug.Log("Level end");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void LoadMenuScene()
    {
        ResetAllFields();
        Debug.Log("Level end");
        SceneManager.LoadScene(menuSceneIndex);
    }

    private void ResetAllFields()
    {
        visitedObjects.Clear();
        LastCheckpoint = Vector3.zero;
        LastCameraConfinerColliderName = String.Empty;
    }

    private IEnumerator WaitAndRealoadScene(int timeBeforeReload)
    {
        transition = FindObjectOfType<TransitionAnimations>();
        if (transition == null)
        {
            Debug.LogError("Game status could not find Transition Canvas");
        }
        transition.TriggerFadeOut();
        yield return new WaitForSeconds(timeBeforeReload);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
