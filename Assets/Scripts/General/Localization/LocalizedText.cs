﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour
{
    private TextMeshProUGUI textComponent;
    public string key;

    // Start is called before the first frame update
    void Awake()
    {
        textComponent = GetComponent<TextMeshProUGUI>();
        textComponent.text = LocalizationManager.instance.GetLocalizedValue(key);
    }

    public void UpdateText(string key) => textComponent.text = LocalizationManager.instance.GetLocalizedValue(key);
}
