﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

public class LocalizationManager : MonoBehaviour
{
    private const string MISSING_TEXT_ERROR = "Localized text not found";
    private const string ENGLISH_LOCALIZED_TEXT_FILE = "localizedText_en.json";
    private const string ENGLISH_LANGUAGE_FULL_NAME = "english";
    private const string ENGLISH_LANGUAGE_SHORT_NAME = "en";

    public static LocalizationManager instance;

    private Dictionary<string, string> localizedText;

    //TODO Need to remember user language after first play.
    private GameLanguage gameLanguage = new GameLanguage(ENGLISH_LOCALIZED_TEXT_FILE, ENGLISH_LANGUAGE_FULL_NAME, ENGLISH_LANGUAGE_SHORT_NAME);
    private bool ready = false;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            LoadLocalizedText(ENGLISH_LOCALIZED_TEXT_FILE);
        } else if (instance != this)
        {
            Destroy(gameObject);
        }

        // This protects object from being destroyed when new scene is loaded.
        DontDestroyOnLoad(gameObject);
    }

    public void LoadLocalizedTextAndReloadStartMenu(string fileName)
    {
        LoadLocalizedText(fileName);
        Destroy(FindObjectOfType<PauseMenu>());
        LoadStartMenu();
    }

    public void LoadLocalizedText(string fileName)
    {
        localizedText = new Dictionary<string, string>();
        string filePath = Path.Combine(Application.streamingAssetsPath, fileName);

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(dataAsJson);

            for (int i = 0; i < loadedData.items.Length; i++)
            {
                localizedText.Add(loadedData.items[i].key, loadedData.items[i].value);
            }

            Debug.Log("Data loaded. Text dictionary contains " + localizedText.Count + " entries");
        }
        else
        {
            Debug.LogError("Cannot find localization file!");
        }

        ready = true;        
    }

    public void LoadStartMenu() => SceneManager.LoadScene("Start Menu");

    public string GetLocalizedValue(string key)
    {
        string result = MISSING_TEXT_ERROR;
        if(localizedText.ContainsKey(key))
        {
            result = localizedText[key];
        }

        return result;
    }

    public bool Ready
    {
        get
        {
            return ready;
        }
    }

    public GameLanguage GameLanguage
    {
        get
        {
            return gameLanguage;
        }

        set
        {
            gameLanguage = value;
        }
    }
}

public struct GameLanguage
{
    private string languageLocalizationTextFile;
    private string languageFullName;
    private string languageShortName;

    public GameLanguage(string languageLocalizationTextFile, string languageFullName, string languageShortName)
    {
        this.languageLocalizationTextFile = languageLocalizationTextFile;
        this.languageFullName = languageFullName;
        this.languageShortName = languageShortName;
    }

    public string LanguageLocalizationTextFile
    {
        get
        {
            return languageLocalizationTextFile;
        }

        set
        {
            languageLocalizationTextFile = value;
        }
    }

    public string LanguageFullName
    {
        get
        {
            return languageFullName;
        }

        set
        {
            languageFullName = value;
        }
    }

    public string LanguageShortName
    {
        get
        {
            return languageShortName;
        }

        set
        {
            languageShortName = value;
        }
    }
}
