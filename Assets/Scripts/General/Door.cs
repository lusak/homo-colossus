﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] private GameObject closedDoors;
    [SerializeField] private GameObject openDoors;
    [SerializeField] private Dialogue dialogue;
    [SerializeField] private bool locked = false;
    [SerializeField] private bool closed = true;

    private Collider2D collider;
    private TextEventsManager textManager;
    private bool keyTaken = false;

    private void Awake()
    {
        collider = GetComponent<Collider2D>();
        textManager = FindObjectOfType<TextEventsManager>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            OpenDoors();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "NPC")
        {
            NPCOpenDoors();
        }
    }

    public void OpenDoors()
    {
        if (!locked && closed)
        {
            closedDoors.SetActive(false);
            openDoors.SetActive(true);
            closed = false;
            collider.enabled = false;
        }

        if (locked && closed)
        {
            if (keyTaken)
            {
                closedDoors.SetActive(false);
                openDoors.SetActive(true);
                closed = false;
                collider.enabled = false;
            } else
            {
                textManager.ProcessDialogue(dialogue, true, false);
            }
        }
    }

    public void NPCOpenDoors()
    {
        closedDoors.SetActive(false);
        openDoors.SetActive(true);
        closed = false;
        collider.enabled = false;
    }

    public void CloseDoors()
    {
        if (!locked && !closed)
        {
            closedDoors.SetActive(true);
            openDoors.SetActive(false);
            closed = true;
            collider.enabled = true;
        }
    }

    public bool KeyTaken
    {
        get
        {
            return keyTaken;
        }

        set
        {
            keyTaken = value;
        }
    }

    public bool Locked
    {
        get
        {
            return locked;
        }

        set
        {
            locked = value;
        }
    }

    public bool Closed
    {
        get
        {
            return closed;
        }

        set
        {
            closed = value;
        }
    }
}
