﻿using UnityEngine;
using System.Collections;

public interface IPlayer
{
    void SetPlayerActive(bool value);
    void LookForward(bool value);
}
