﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class YarnContinueButton : MonoBehaviour
{
    [SerializeField] private Text text;

    private Button button;

    private void Awake() => button = GetComponent<Button>();

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            button.onClick.Invoke();
        }
    }
}
