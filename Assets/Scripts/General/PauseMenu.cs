﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static PauseMenu instance;

    public static bool GamePaused = false;
    public const int startMenuSceneIndex = 1;

    public GameObject pauseMenuUI;
    public GameObject optionsMenu;
    public GameObject pauseMenu;

    private CursorManager cursorManager;
    private bool ingameCursorIsLocked;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        } else if (this != instance)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        cursorManager = FindObjectOfType<CursorManager>();

        if (pauseMenuUI == null)
        {
            Debug.LogError("No pause menu selected");
        }
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (SceneManager.GetActiveScene().buildIndex > 1)
            {
                ingameCursorIsLocked = CursorManager.lockedCursor;
                if (GamePaused)
                {
                    if (ingameCursorIsLocked)
                    {
                        cursorManager.SetLockedCursor();
                    }
                    Resume();
                }
                else
                {
                    cursorManager.SetMovableCursor();
                    Pause();
                }
            }
        }
    }

    public void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GamePaused = true;
    }

    public void Resume()
    {
        //journal active false
        optionsMenu.SetActive(false);
        pauseMenu.SetActive(true);
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GamePaused = false;
    }

    public void LoadMainMenu()
    {
        Resume();
        SceneManager.LoadScene(startMenuSceneIndex);
    }

    public void QuitGame() => Application.Quit();
}
