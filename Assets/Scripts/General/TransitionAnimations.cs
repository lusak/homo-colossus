﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionAnimations : MonoBehaviour
{
    private const string FADE_OUT_TRIGGER = "FadeOut";
    private const string FADE_IN_TRIGGER = "FadeIn";

    private Animator animator;

    private void Awake() => animator = GetComponent<Animator>();

    private void Start()
    {
        if (animator == null)
        {
            Debug.LogError("No animator component found");
        }
    }

    public void TriggerFadeIn() => animator.SetTrigger(FADE_IN_TRIGGER);

    public void TriggerFadeOut() => animator.SetTrigger(FADE_OUT_TRIGGER);
}
