﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapEndEvent : MonoBehaviour
{
    [SerializeField] GameObject characterToEnd;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == characterToEnd.gameObject)
        {
            GameStatus.instance.LoadNextScene();
        }
    }
}
