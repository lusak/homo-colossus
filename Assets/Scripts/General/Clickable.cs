﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clickable : MonoBehaviour
{
    private CursorManager cursorManager;

    private void OnMouseEnter() => CursorManager.instance.SetHighletedCursor();

    private void OnMouseExit() =>CursorManager.instance.SetDefaultCursor();
}
