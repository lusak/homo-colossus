﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TextEvent : MonoBehaviour
{
    [SerializeField] private Dialogue dialogue;
    [SerializeField] private bool playerLookForward = true;
    [SerializeField] private bool deactivatePlayer = true;

    private TextEventsManager eventManager;
    private RegistrableObject registrable;

    private void Awake()
    {
        eventManager = FindObjectOfType<TextEventsManager>();
        registrable = GetComponent<RegistrableObject>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (registrable == null)
        {
            Debug.LogError("Missing registrable script");
            return;
        }

        if (collision.gameObject.tag == "Player")
        {
            if (registrable.OneTimeEvent && registrable.AlreadyVisited())
            {
                return;
            }

            if (!registrable.AlreadyVisited())
            {
                registrable.MarkAsVisited();
            }

            eventManager.ProcessDialogue(dialogue, deactivatePlayer, playerLookForward);
        }
    }
}
