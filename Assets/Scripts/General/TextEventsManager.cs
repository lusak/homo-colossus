﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TextEventsManager : MonoBehaviour
{
    [SerializeField] private TMPro.TextMeshProUGUI textBox;
    [SerializeField] private GameObject textBoxBackground;

    private IPlayer player;
    private LocalizationManager localizationManager;
    private CursorManager cursorManager;

    private Queue<string> dialogue = new Queue<string>();

    private void Awake()
    {
        localizationManager = FindObjectOfType<LocalizationManager>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<IPlayer>();
        cursorManager = FindObjectOfType<CursorManager>();
        cursorManager.SetMovableCursor();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (textBox.IsActive())
            {
                NextSentence();
            }
        }
    }

    public void ProcessDialogue(Dialogue dialogue, bool deactivatePlayer, bool playerLookForward)
    {
        if (playerLookForward) { player.LookForward(true); }
        if (deactivatePlayer) { player.SetPlayerActive(false); }
        PopulateDialogue(dialogue);
        textBoxBackground.SetActive(true);
        NextSentence();
    }

    public void NextSentence()
    {
        if (dialogue.Count == 0)
        {
            textBoxBackground.SetActive(false);
            player.SetPlayerActive(true);
        }
        else
        {
            string key = dialogue.Dequeue();
            textBox.text = localizationManager.GetLocalizedValue(key);
        }
    }

    private void PopulateDialogue(Dialogue dialogue)
    {
        foreach (string key in dialogue.sentencesKeys)
        {
            this.dialogue.Enqueue(key);
        }
    }
}
