﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorManager : MonoBehaviour
{
    [SerializeField] private Texture2D defaultCursorIcon;
    [SerializeField] private Texture2D highlightCursorIcon;

    public static CursorManager instance;
    public static bool lockedCursor = false;
    private void Awake()
    {
        if (!instance)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public void SetDefaultCursor()
    {
        Cursor.SetCursor(defaultCursorIcon, Vector2.zero, CursorMode.Auto);
        ShowCursor();
    }

    public void SetHighletedCursor()
    {
        Cursor.SetCursor(highlightCursorIcon, Vector2.zero, CursorMode.Auto);
        ShowCursor();
    }

    public void SetMovableCursor()
    {
        Cursor.SetCursor(defaultCursorIcon, Vector2.zero, CursorMode.Auto);
        Cursor.lockState = CursorLockMode.None;
        ShowCursor();
        lockedCursor = false;
    }

    public void SetLockedCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
        ShowCursor();
        lockedCursor = true;
    }

    public void HideCursor() => Cursor.visible = false;

    public void ShowCursor() => Cursor.visible = true;
}
