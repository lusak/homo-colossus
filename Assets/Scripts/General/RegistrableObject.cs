﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegistrableObject : MonoBehaviour
{
    [SerializeField] private bool oneTimeEvent;

    public void MarkAsVisited() => GameStatus.instance.registerVisitedObject(transform.position);

    public bool AlreadyVisited() => GameStatus.instance.checkIfObjectVisited(transform.position);
    public bool OneTimeEvent
    {
        get
        {
            return oneTimeEvent;
        }
    }
}
