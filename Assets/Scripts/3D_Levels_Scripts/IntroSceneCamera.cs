﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class IntroSceneCamera : MonoBehaviour
{
    [SerializeField] private float shakeMagnitude;
    [SerializeField] private float shakeDuration;
    [SerializeField] private float burnDuration;

    public IEnumerator Shake()
    {
        Vector3 startPosition = transform.localPosition;
        float elapsed = 0f;

        while (elapsed < shakeDuration)
        {
            float x = Random.Range(-1f, 1f) * shakeMagnitude;
            float y = Random.Range(-1f, 1f) * shakeMagnitude;

            transform.localPosition = new Vector3(x, y, startPosition.z);
            elapsed += Time.deltaTime;
            yield return null;
        }
        transform.localPosition = startPosition;
    }

    public IEnumerator StartBurning()
    {
        RenderSettings.fog = true;
        float elapsed = 0f;
        while (elapsed < burnDuration)
        {
            RenderSettings.fogDensity += 0.0003f;
            elapsed += Time.deltaTime;
            yield return null;
        }
    }
}
