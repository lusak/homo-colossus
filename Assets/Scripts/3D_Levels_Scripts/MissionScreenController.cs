﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionScreenController : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    private MissionController missionController;
    
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        missionController = FindObjectOfType<MissionController>();
        EnableRenderer(false);
    }
    private void OnMouseDown() => missionController.ProgressMission();

    public void EnableRenderer(bool value) => spriteRenderer.enabled = value;

}
