﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlayerMovement3D : MonoBehaviour
{

    [SerializeField] public float mouseSensitivity = 5f;
    [SerializeField] float moveSpeed = 12f;

    public CharacterController controller;

    private bool active = true;

    private Transform startTransform;

    private Vector3 startPosition;
    private float xRotation = 0f;
    private float yRotation = 0f;

    private void Start()
    {
        startPosition = transform.position;
        startTransform = transform;
    }

    void Update()
    {
        if (active)
        {
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");

            Vector3 flyMode = transform.right * x + transform.forward * z;
            Vector3 walk = new Vector3(flyMode.x, 0f, flyMode.z);

            controller.Move(walk * moveSpeed * Time.deltaTime);

            float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity;
            float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity;

            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, -45f, 45f);

            yRotation += mouseX;
            transform.localRotation = Quaternion.Euler(xRotation, yRotation, 0f);
        }
    }

    public void MoveToStartPosition()
    {
        //Put some scene "clip", turn camere off and on again
        transform.position = startPosition;
        transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    public bool Active
    {
        get
        {
            return active;
        }
        set
        {
            active = value;
        }
    }
}
