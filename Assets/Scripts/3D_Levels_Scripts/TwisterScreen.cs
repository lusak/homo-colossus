﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwisterScreen : MonoBehaviour
{
    public AudioSource audioSource;

    private void OnMouseDown()
    {
        if (audioSource.isPlaying)
        {
            audioSource.Stop();
        }
        else
        {
            audioSource.Play();
        }
    }
}
