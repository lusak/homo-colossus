﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopButtonUIWorkaround : MonoBehaviour
{
    private MissionController missionController;

    private void Awake() => missionController = FindObjectOfType<MissionController>();

    private void OnMouseDown() => missionController.DeclineMission();

    private void OnMouseEnter() => CursorManager.instance.SetHighletedCursor();

    private void OnMouseExit() => CursorManager.instance.SetDefaultCursor();
}
