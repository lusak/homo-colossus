﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControllerIsometric : MonoBehaviour, IPlayer
{
    [SerializeField] private float moveSpeed = 0.1f;
    
    private bool active = true;
    
    private Rigidbody2D rBody;
    private Animator animator;

    private Vector3 movement;
    private bool moved = false;

    private void Awake()
    {
        rBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (active)
        {
            Move();
        }
    }

    private void FixedUpdate()
    {
        if (moved)
        {
            rBody.MovePosition(transform.position + movement * moveSpeed);
            moved = false;
        }
    }

    public void DeactivatePlayer()
    {
        animator.SetBool("moving", false);
        active = false;
    }

    public void SetPlayerActive(bool value)
    {
        if (value)
        {
            active = true;

        } else
        {
            active = false;
            animator.SetBool("moving", false);
        }
    }

    public void SetAnimatorFloatValue(string property, float value) => animator.SetFloat(property, value);
 
    public void SetAnimatorBoolValue(string property, bool value) => animator.SetBool(property, value);

    public IEnumerator SpriteBlinking(float time)
    {
        animator.SetBool("wet", true);
        yield return new WaitForSeconds(time);
        animator.SetBool("wet", false);
    }

    public bool Active
    {
        get
        {
            return active;
        }

        set
        {
            active = value;
        }
    }

    private void Move()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        if (x != 0 || y != 0)
        {
            movement = new Vector3(x, y, 0);
            moved = true;
            animator.SetBool("moving", true);
            animator.SetFloat("moveX", x);
            animator.SetFloat("moveY", y);
        }
        else
        {
            animator.SetBool("moving", false);
        }
    }

    public void LookForward(bool value)
    {
        //Not needed in Isometric view
    }
}
