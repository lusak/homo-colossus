﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCControllerISO : MonoBehaviour
{
    private float moveSpeed = 3f;

    private Animator animator;

    private bool moving;
    private Vector3 destination;


    private void Awake() => animator = GetComponent<Animator>();


    private void Update()
    {
        if (moving)
        {
            float x = destination.x - transform.position.x;
            float y = destination.y - transform.position.y;

            animator.SetFloat("moveX", x);
            animator.SetFloat("moveY", y);

            transform.position = Vector3.MoveTowards(transform.position, destination, moveSpeed * Time.deltaTime);

            if (Vector3.Distance(transform.position, destination) < 0.01f)
            {
                moving = false;
                animator.SetBool("moving", false);
            }
        }
    }

    public void MoveTowards(Vector3 dest)
    {
        destination = dest;
        animator.SetBool("moving", true);
        moving = true;
    }

    public void SetAnimatorFloatValue(string property, float value) => animator.SetFloat(property, value);

    public void SetAnimatorBoolValue(string property, bool value) => animator.SetBool(property, value);

    public IEnumerator WaitForFinishedMoving()
    {
        while (moving)
        {
            yield return null;
        }
    }

    //rotSpeed is angle rotated per frame
    public IEnumerator RotateCharacter(float finalZrotation, float rotSpeed)
    {
        float rotation = transform.localRotation.z;
        if (finalZrotation - rotation >= 0)
        {
            while (finalZrotation - rotation > rotSpeed)
            {
                rotation += rotSpeed;
                transform.localRotation = Quaternion.Euler(0f, 0f, rotation);
                yield return null;
            }
        }
        else
        {
            while (rotation - finalZrotation > 0f)
            {
                rotation += rotSpeed;
                transform.localRotation = Quaternion.Euler(0f, 0f, -rotation);
                yield return null;
            }
        }
    }

    public float MoveSpeed
    {
        get
        {
            return moveSpeed;
        }

        set
        {
            moveSpeed = value;
        }
    }
}
