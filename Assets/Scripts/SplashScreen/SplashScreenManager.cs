﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreenManager : MonoBehaviour
{
    private static int SPLASH_SCREEN_ANIMATION_DURATION = 7;
    private LocalizationManager localizationManager;

    IEnumerator Start()
    {
        localizationManager = FindObjectOfType<LocalizationManager>();
        yield return new WaitForSeconds(SPLASH_SCREEN_ANIMATION_DURATION);

        while(!localizationManager.Ready)
        {
            yield return null;
        }

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
