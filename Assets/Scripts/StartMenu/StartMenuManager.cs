﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMenuManager : MonoBehaviour
{
    private CursorManager cursorManager;
    void Start()
    {
        cursorManager = FindObjectOfType<CursorManager>();
        cursorManager.SetMovableCursor();
    }

    public void Quit() => StartCoroutine(WaitAndQuit());

    public void StartNewGame() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

    private IEnumerator WaitAndQuit()
    {
        yield return new WaitForSeconds(3);
        Application.Quit();
    }
}
