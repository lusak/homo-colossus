﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageButtonManager : MonoBehaviour
{
    private const string ENGLISH_LOCALIZATION_FILE = "localizedText_en.json";
    private const string POLISH_LOCALIZATION_FILE = "localizedText_pl.json";

    public void changeLanguage(string language)
    {
        LocalizationManager localizationManager = FindObjectOfType<LocalizationManager>();
        GameStatus gameStatus = FindObjectOfType<GameStatus>();
        switch (language)
        {
            case "polish":
                localizationManager.LoadLocalizedTextAndReloadStartMenu(POLISH_LOCALIZATION_FILE);
                LocalizationManager.instance.GameLanguage = new GameLanguage(POLISH_LOCALIZATION_FILE, language, "pl-PL");
                break;
            case "english":
                localizationManager.LoadLocalizedTextAndReloadStartMenu(POLISH_LOCALIZATION_FILE);
                LocalizationManager.instance.GameLanguage = new GameLanguage(ENGLISH_LOCALIZATION_FILE, language, "en");
                break;
            default:
                Debug.LogError("Language " + language + " not supported");
                break;
        }
    }
}
