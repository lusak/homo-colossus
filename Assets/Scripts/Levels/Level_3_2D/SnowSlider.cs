﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowSlider : MonoBehaviour, IPlayer
{
    [SerializeField] private float moveSpeed = 0.3f;
    [SerializeField] private float maxSpeed = 10f;
    [SerializeField] private float moveSidewaysMinSpeedFactor = 0.1f;
    [SerializeField] private float minTimeBetweenHits = 1f;
    [SerializeField] private int maxHealth = 100;
    [SerializeField] private int health;

    [SerializeField] GameObject cameraFollowObject;

    [SerializeField] AudioSource engineWorking;
    [SerializeField] AudioSource engineIdle;
   
    private HealthBar healthBar;
    private Rigidbody2D rBody;
    private Collider2D collider;
    private Animator animator;

    private Vector2 movement;
    private bool moved = false;
    private bool active = true;
    private bool engineRunning = false;
    private bool dead = false;
    private float lastTimeHit = 0f;
    private bool isTouchingWater = false;
    private float startedTouchingWater = 0f;
    private float timeTouchingWaterBeforeSinking = 1f;

    private void Awake()
    {
        rBody = GetComponent<Rigidbody2D>();
        collider = GetComponent<Collider2D>();
        animator = GetComponent<Animator>();
        healthBar = FindObjectOfType<HealthBar>();
    }

    private void Start()
    {
        healthBar.SetMaxHealth(maxHealth);
        health = maxHealth;

        if (GameStatus.instance.LastCheckpoint != Vector3.zero)
        {
            transform.position = GameStatus.instance.LastCheckpoint;
        }
    }

    void Update()
    {
        if (active)
        {
            Move();
            EngineSound();
        }

        if (active && collider.IsTouchingLayers(LayerMask.GetMask("Water"))) {
            if (!isTouchingWater)
            {
                isTouchingWater = true;
                startedTouchingWater = Time.time;
            } else
            {
                if (Time.time - startedTouchingWater > timeTouchingWaterBeforeSinking)
                {
                    active = false;
                    GameStatus.instance.ReloadScene(1);
                }
            }
        } else
        {
            isTouchingWater = false;
            startedTouchingWater = 0f;
        }
    }

    private void FixedUpdate()
    {
        if (moved)
        {
            if (rBody.velocity.magnitude < maxSpeed)
            {
                rBody.velocity += movement * moveSpeed;
            }
            moved = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (Time.time - lastTimeHit < minTimeBetweenHits)
        {
            return;
        }

        if (collision.gameObject.tag == "NPC")
        {
            TakeDamage(Mathf.RoundToInt(maxHealth * 0.2f));
            lastTimeHit = Time.time;
        }
        else
        {
            if (rBody.velocity.magnitude > maxSpeed * 0.8f)
            {
                TakeDamage(Mathf.RoundToInt(maxHealth * 0.2f));
                lastTimeHit = Time.time;
            }
            else if (rBody.velocity.magnitude > maxSpeed * 0.5f)
            {
                TakeDamage(Mathf.RoundToInt(maxHealth * 0.1f));
                lastTimeHit = Time.time;
            }
        }

        if (!dead && health <= 0)
        {
            dead = true;
            Debug.Log("Game over");
            GameStatus.instance.ReloadScene(3);
        }
    }

    public void MoveCameraFollowObject(Vector3 shift) => cameraFollowObject.transform.localPosition += shift;

    public void SetPlayerActive(bool value) => active = value;

    public void ResetVelocity() => rBody.velocity = Vector2.zero;

    public void MuteSounds()
    {
        engineIdle.Stop();
        engineWorking.Stop();
    }

    public IEnumerator MoveWithTransForm(Vector3 destination, float speed)
    {
        while (Vector3.Distance(transform.position, destination) > 0.01f)
        {
            transform.position = Vector3.MoveTowards(transform.position, destination, speed * Time.deltaTime);
            yield return null;
        }
    }

    public bool Active
    {
        get
        {
            return active;
        }

        set
        {
            active = value;
        }
    }

    public HealthBar HealthBar
    {
        get
        {
            return healthBar;
        }
    }

    private void TakeDamage(int damage)
    {
        health -= damage;
        healthBar.SetHealth(health);
    }

    private void Move()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        if (y != 0 || rBody.velocity.y > maxSpeed * moveSidewaysMinSpeedFactor)
        {
            movement = new Vector2(x, y);
            moved = true;
            animator.SetFloat("moveX", x);
            engineRunning = true;
        }
        else if (x != 0 && y < maxSpeed * moveSidewaysMinSpeedFactor)
        {
            engineRunning = false;
        }
        else
        {
            engineRunning = false;
        }
    }

    private void EngineSound()
    {
        if (engineRunning)
        {
            if (!engineWorking.isPlaying)
            {
                engineWorking.Play();
            }

            if (engineIdle.isPlaying)
            {
                engineIdle.Stop();
            }
        } else
        {
            if (!engineIdle.isPlaying)
            {
                engineIdle.Play();
            }

            if (engineWorking.isPlaying)
            {
                engineWorking.Stop();
            }
        }
    }

    public void LookForward(bool value)
    {
        //Not needed
    }
}
