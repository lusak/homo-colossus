﻿using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using UnityEngine;

public class NPCSnowSlider : MonoBehaviour
{
    [SerializeField] GameObject chaseStartPosition;
    [SerializeField] private float moveSpeed = 10f;
    private Animator animator;
    private AIPath aPath;

    private void Awake()
    {
        animator = GetComponentInChildren<Animator>();
        aPath = GetComponent<AIPath>();
        aPath.enabled = false;
    }
    void Update()
    {
        if(Mathf.Abs(aPath.desiredVelocity.x) > 1f)
        {
            animator.SetFloat("moveX", aPath.desiredVelocity.x);
        } else
        {
            animator.SetFloat("moveX", 0f);
        }
    }

    public void ActivateAPath(bool value)
    {
        aPath.enabled = value;
    }

    public IEnumerator MoveToStartPosition()
    {
        while (Vector3.Distance(transform.position, chaseStartPosition.transform.position) > 0.01f)
        {
            transform.position = Vector3.MoveTowards(transform.position, chaseStartPosition.transform.position, moveSpeed * Time.deltaTime);
            yield return null;
        }
    }

    public IEnumerator RotateSliderToNormalPosition()
    {
        float rotation = transform.localRotation.z;
        {
            while (rotation > 0f)
            {
                rotation -= 0.1f;
                transform.localRotation = Quaternion.Euler(0f, 0f, rotation);
                yield return null;
            }
        }

    }
}
