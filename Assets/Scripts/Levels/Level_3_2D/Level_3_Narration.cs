﻿using System.Collections;
using System.Collections.Generic;
using Yarn.Unity;
using UnityEngine;
using UnityEngine.SceneManagement;
using Cinemachine;

public class Level_3_Narration : MonoBehaviour
{
    private const string YARN_DIALOGUE_START_LEVEL = "Level3_Dialogue0_Start";
    private const string YARN_DIALOGUE_START_CHASE = "Level3_Dialogue1_Start";
    private const string YARN_DIALOGUE_SPACESHIP = "Level3_Dialogue2_Start";

    [SerializeField] GameObject sliderStop;
    [SerializeField] GameObject spaceshipEnterence2;
    [SerializeField] GameObject spaceshipEnterence;
    [SerializeField] GameObject roadBlockade;
    [SerializeField] GameObject roadBlockadeInfo;

    [SerializeField] NPCControllerISO helena;
    [SerializeField] NPCControllerISO player;
    [SerializeField] GameObject helenaInSlider;
    [SerializeField] GameObject playerInSlider;
    [SerializeField] GameObject ship;
    [SerializeField] GameObject shipChangeDirection;

    [SerializeField] CinemachineVirtualCamera vCamera;

    [SerializeField] private float sliderStoppingSpeed = 6f;
    [SerializeField] private float spaceShipSpeed = 5f;

    private SnowSlider slider;
    private Thrusters shipThrusters;
    private NPCSnowSlider npcSlider;
    private DialogueRunner yarnRunner;
    private TransitionAnimations transition;
    private int levelReloadDelay = 2;

    private bool chaseTriggered = false;

    private void Awake()
    {
        slider = FindObjectOfType<SnowSlider>();
        shipThrusters = FindObjectOfType<Thrusters>();
        yarnRunner = FindObjectOfType<DialogueRunner>();
        npcSlider = FindObjectOfType<NPCSnowSlider>();
        transition = FindObjectOfType<TransitionAnimations>();
    }

    public void NpcShowsUp() {
        slider.SetPlayerActive(false);
        slider.MuteSounds();
        yarnRunner.StartDialogue(YARN_DIALOGUE_START_CHASE);
    }

    public void LevelStartDialogue() => yarnRunner.StartDialogue(YARN_DIALOGUE_START_LEVEL);

    public void SpaceShipFound() => StartCoroutine(SpaceshipFoundCoroutine());

    public void ChaseEnd()
    {
        roadBlockade.SetActive(true);
        roadBlockadeInfo.SetActive(true);
    }

    [YarnCommand("spaceshipDeparts")]
    public void SpaceshipDeparts() => StartCoroutine(SpaceshipDepartsCoroutine());

    public IEnumerator SpaceshipDepartsCoroutine()
    {
        transition.TriggerFadeOut();
        yield return new WaitForSeconds(2);
        helena.gameObject.SetActive(false);
        player.gameObject.SetActive(false);
        vCamera.m_Lens.OrthographicSize = 20;
        transition.TriggerFadeIn();
        yield return new WaitForSeconds(2);
        shipThrusters.StartCoroutine(shipThrusters.StartThrusters());
        yield return new WaitForSeconds(2);
        while (Vector3.Distance(ship.transform.position, shipChangeDirection.transform.position) > 0.1f)
        {
            ship.transform.position = Vector3.MoveTowards(ship.transform.position, shipChangeDirection.transform.position, spaceShipSpeed * Time.deltaTime);
            if (ship.transform.localRotation.eulerAngles.z > -180f)
            {
                ship.transform.Rotate(0, 0, -0.02f);
            }
            spaceShipSpeed += 0.01f;
            yield return null;
        }
        Debug.Log("End level");
        GameStatus.instance.LoadNextScene();

    }

    [YarnCommand("moveToStartPosition")]
    public void NPCMoveToStartPosition() => npcSlider.StartCoroutine(npcSlider.MoveToStartPosition());

    [YarnCommand("startChase")]
    public void StartChase()
    {
        if (chaseTriggered)
        {
            return;
        }
        slider.SetPlayerActive(true);
        npcSlider.ActivateAPath(true);
        npcSlider.StartCoroutine(npcSlider.RotateSliderToNormalPosition());
        chaseTriggered = true;
    }

    [YarnCommand("finishChase")]
    public void FinishChase() => npcSlider.ActivateAPath(false);

    private IEnumerator SpaceshipFoundCoroutine()
    {
        slider.Active = false;
        slider.ResetVelocity();
        yield return slider.StartCoroutine(slider.MoveWithTransForm(sliderStop.transform.position, sliderStoppingSpeed));
        slider.MuteSounds();
        transition.TriggerFadeOut();
        yield return new WaitForSeconds(2);
        slider.HealthBar.Hide();
        helena.gameObject.SetActive(true);
        player.gameObject.SetActive(true);
        HidePassangers();
        transition.TriggerFadeIn();
        yield return new WaitForSeconds(2);
        helena.MoveTowards(spaceshipEnterence2.transform.position);
        player.MoveTowards(spaceshipEnterence2.transform.position - new Vector3(0,1,0));
        yield return helena.StartCoroutine(helena.WaitForFinishedMoving());
        helena.MoveTowards(spaceshipEnterence.transform.position);
        yarnRunner.StartDialogue(YARN_DIALOGUE_SPACESHIP);
    }

    private void HidePassangers()
    {
        helenaInSlider.SetActive(false);
        playerInSlider.SetActive(false);
    }
}
