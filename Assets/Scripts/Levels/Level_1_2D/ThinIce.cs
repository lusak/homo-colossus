﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThinIce : MonoBehaviour
{
    private Vector3 startPosition;
    private float lastTimeTouched;
    private float sinkingSpeed = 5f;


    private void Awake() => startPosition = transform.position;


    private void Update()
    {
        if (transform.position.y < startPosition.y)
        {
            if (Time.time - lastTimeTouched > 0.3)
            {
                transform.position += Vector3.up * Time.deltaTime * sinkingSpeed;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        lastTimeTouched = Time.time;
        transform.position -= Vector3.up / 100;
    }
}
