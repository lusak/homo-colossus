﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableIce : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private float secondsBeforeFall = 1f;
    [SerializeField] private bool destroyAfterTrigger = false; 

    private Rigidbody2D rigidbody;

    private void Awake() => rigidbody = GetComponent<Rigidbody2D>();

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            StartCoroutine(BreakAndFall());
        }
    }

    private IEnumerator BreakAndFall()
    {
        audioSource.Play();
        yield return new WaitForSeconds(secondsBeforeFall);
        rigidbody.bodyType = RigidbodyType2D.Dynamic;
        if (destroyAfterTrigger)
        {
            yield return new WaitForSeconds(1f);
            Destroy(gameObject);
        }
    }
}
