﻿using UnityEngine;
using Cinemachine;
using System.Collections;

public class Level_1_Narration : MonoBehaviour
{
    [SerializeField] private NPCController2D helena;
    [SerializeField] private NPCController2D tomas;
    [SerializeField] private NPCController2D john;
    [SerializeField] private GameObject mapEnd;
    [SerializeField] private CinemachineConfiner cameraConfiner;
    [SerializeField] private GameObject mainCamera;

    //WaterfallEvent
    [SerializeField] private GameObject waterfallDestination;
    [SerializeField] private float rotation = 90f;
    [SerializeField] private float rotationSpeed = 0.7f;
    [SerializeField] private float blinkTime = 2f;

    public Dialogue dialogue1;
    public Dialogue dialogue2;
    public Dialogue dialogue3;

    private CharacterController2D character;
    private TextEventsManager textManager;
    private bool alreadyTriggered = false;

    private CursorManager cursorManager;
    private TransitionAnimations transition;

    private void Awake()
    {
        textManager = FindObjectOfType<TextEventsManager>();
        character = FindObjectOfType<CharacterController2D>();
        cursorManager = FindObjectOfType<CursorManager>();
        cursorManager.SetMovableCursor();
        transition = FindObjectOfType<TransitionAnimations>();

        if (!string.IsNullOrEmpty(GameStatus.instance.LastCameraConfinerColliderName))
        {
            CameraConfiner[] confiners = FindObjectsOfType<CameraConfiner>();
            foreach (CameraConfiner confiner in confiners)
            {
                if (confiner.name == GameStatus.instance.LastCameraConfinerColliderName)
                {
                    cameraConfiner.m_BoundingShape2D = confiner.collider;
                }
            }
        }
    }

    public void MeetScientists() => StartCoroutine(MeetScientistsCoroutine());


    public void ChangeCameraConfinerCollider(Collider2D collider)
    {
        GameStatus.instance.LastCameraConfinerColliderName = collider.gameObject.name;
        cameraConfiner.m_BoundingShape2D = collider;
    }

    public void ProcessTeleport(Vector3 destination, Collider2D cameraCollider)
    {
        character.Active = false;
        character.DontSimuleteRigidBody();
        character.LookForward(true);

        StartCoroutine(Teleport(destination, cameraCollider));
    }

    public void PlayerGetWet()
    {
        character.StartCoroutine(character.SpriteBlinking(2f));
    }

    public IEnumerator Teleport(Vector3 destination, Collider2D cameraCollider)
    {
        mainCamera.SetActive(false);
        transition.TriggerFadeOut();
        yield return new WaitForSecondsRealtime(2);
        character.transform.position = destination;
        ChangeCameraConfinerCollider(cameraCollider);
        character.SimuleteRigidBody();
        mainCamera.SetActive(true);
        yield return new WaitForSecondsRealtime(2);
        transition.TriggerFadeIn();
        character.Active = true;
    }

    public void TriggerWaterFallEvent()
    {
        StartCoroutine(Waterfall());
    }

    private IEnumerator Waterfall()
    {
        character.Active = false;
        character.StartCoroutine(character.RotateCharacter(rotation, rotationSpeed));
        character.StartCoroutine(character.SpriteBlinking(blinkTime));
        yield return character.StartCoroutine(character.MoveInactivePlayerByRigidBody(waterfallDestination.transform.position.x));
    }

    private IEnumerator MeetScientistsCoroutine()
    {
        yield return StartCoroutine(MeetScientistsPart1());
        textManager.ProcessDialogue(dialogue1, true, false);
        yield return character.StartCoroutine(character.WaitForPlayerActiveThenDeactivate());
        yield return StartCoroutine(MeetScientistsPart2());
        textManager.ProcessDialogue(dialogue2, true, false);
        yield return character.StartCoroutine(character.WaitForPlayerActiveThenDeactivate());
        yield return StartCoroutine(MeetScientistsPart3());
        textManager.ProcessDialogue(dialogue3, true, false);
        character.StartCoroutine(character.WaitForPlayerActiveThenDeactivate());
        yield return new WaitForSeconds(2f);
        MeetScientistsPart4();

    }
    private IEnumerator MeetScientistsPart1()
    {
        //Wait untill character stops moving
        tomas.FlipSprite();
        yield return new WaitForSeconds(0.5f);
        john.FlipSprite();
        yield return new WaitForSeconds(1f);

        john.StartCoroutine(john.Move(character.transform.position.x + 0.4f, 0.015f));
        yield return tomas.StartCoroutine(tomas.Move(character.transform.position.x - 0.6f, 0.02f));
        tomas.FlipSprite();
        yield return helena.StartCoroutine(helena.Move(character.transform.position.x + 1f, 0.02f));
    }

    private IEnumerator MeetScientistsPart2()
    {
        john.Kneel(true);
        yield return new WaitForSeconds(2f);
        yield return character.StartCoroutine(character.SpriteBlinking(2f));
    }
    private IEnumerator MeetScientistsPart3()
    {
        tomas.transform.position -= Vector3.up * 0.1f;
        tomas.Kneel(true);
        yield return new WaitForSeconds(1f);
        tomas.Kneel(false);
        tomas.transform.position += Vector3.up * 0.1f;
        john.Kneel(false);
        character.DontSimuleteRigidBody();
        character.transform.position += Vector3.up * 0.2f;
        character.StartCoroutine(character.MoveInactivePlayerByTransform(mapEnd.transform.position.x, 0.01f));
        john.StartCoroutine(john.Move(mapEnd.transform.position.x, 0.01f));
        tomas.StartCoroutine(tomas.Move(mapEnd.transform.position.x, 0.01f));
        yield return new WaitForSeconds(3f);

    }

    private void MeetScientistsPart4() => helena.StartCoroutine(helena.Move(mapEnd.transform.position.x + 1, 0.025f));
}
