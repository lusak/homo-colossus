﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

public class Level_2_Narration : MonoBehaviour
{
    private const string YARN_DIALOGUE_TOMAS_SCARED = "Level2_Dialogue2_Start";
    private const string YARN_DIALOGUE_BOILER_EVENT = "Level2_Dialogue3_Start";
    private const string YARN_DIALOGUE_COMPUTER_EVENT = "Level2_Dialogue4_Start";
    private const string YARN_DIALOGUE_JOE_LEAVES = "Level2_Dialogue5_Start";
    private const string YARN_DIALOGUE_ESCAPE_ROOM = "Level2_Dialogue6_Start";
    private const string YARN_DIALOGUE_GAME_OVER = "Level2_GameOver_Start";

    [SerializeField] private NPCControllerISO helena;
    [SerializeField] private NPCControllerISO tomas;
    [SerializeField] private NPCControllerISO john;
    [SerializeField] private NPCControllerISO joe;

    [SerializeField] private GameObject startingRoomExit;
    [SerializeField] private GameObject startingRoomInside0;
    [SerializeField] private GameObject startingRoomInside1;
    [SerializeField] private GameObject startingRoomInside2;
    [SerializeField] private GameObject startingRoomInside3;
    [SerializeField] private GameObject startingRoomInside4;
    [SerializeField] private GameObject startingRoomInside5;
    [SerializeField] private Door startingRoomDoors;
    [SerializeField] private Door helenaRoomDoors;
    [SerializeField] private GameObject boiler;
    [SerializeField] private GameObject helenaRoomEntrance;
    [SerializeField] private GameObject helenaRoomInside;
    [SerializeField] private GameObject helenaRoomInside1;
    [SerializeField] private GameObject helenaRoomInside2;
    [SerializeField] private GameObject helenaRoomInside3;
    [SerializeField] private GameObject helenaBed;
    [SerializeField] private GameObject escapeRoomInside;
    [SerializeField] private GameObject escapeRoomInside1;
    [SerializeField] private GameObject escapeWall;
    [SerializeField] private GameObject escapeFloor;
    [SerializeField] private FogOfWar escapeRoomFog;
    [SerializeField] private FogOfWar helenaRoomFog;
    [SerializeField] private Level_2_Window escapeRoomWindow;
    [SerializeField] private GameObject hideInWardrobe;
    [SerializeField] private GameObject escapeRoom;
    [SerializeField] private GameObject snowSlider;

    [SerializeField] private AudioSource breakWallClip;
    [SerializeField] private AudioSource openWindowClip;
    [SerializeField] private AudioSource footstepsClip;

    private DialogueRunner yarnRunner;
    private CharacterControllerIsometric character;
    private TransitionAnimations transition;

    private Vector3 moveBedVector = new Vector3(1.1f, -0.8f);
    private float moveBedSpeed = 1f;

    private bool playerDry = false;
    private bool playerHidden = false;
    private bool tomasScared = false;

    private void Awake()
    {
        yarnRunner = FindObjectOfType<DialogueRunner>();
        character = FindObjectOfType<CharacterControllerIsometric>();
        transition = FindObjectOfType<TransitionAnimations>();
    }

    private void Start()
    {
        character.Active = false;
        tomas.SetAnimatorFloatValue("moveX", -1f);
        john.SetAnimatorFloatValue("moveX", 1f);
    }


    [YarnCommand("helenaEnter")]
    public void HelenaEnter() => helena.MoveTowards(startingRoomInside0.transform.position);

    [YarnCommand("leaveRoom1")]
    public void LeaveRoom1()
    {
        helena.MoveTowards(startingRoomExit.transform.position);
        tomas.MoveTowards(startingRoomExit.transform.position);
        john.MoveTowards(startingRoomExit.transform.position);
        joe.MoveTowards(startingRoomExit.transform.position);
    }

    [YarnCommand("joeLeaveRoom")]
    public void JoeLeaveRoom() => joe.MoveTowards(startingRoomExit.transform.position);

    [YarnCommand("johnLeaveRoom")]
    public void JohnLeaveRoom() => john.MoveTowards(startingRoomExit.transform.position);

    [YarnCommand("helenaLeaveRoom")]
    public void HelenaLeaveRoom() => helena.MoveTowards(startingRoomExit.transform.position);

    [YarnCommand("allEnterRoom")]
    public void allEnterRoom() => StartCoroutine(allEnterRoomCoroutine());

    [YarnCommand("closeStartingRoomDoors")]
    public void CloseStartingRoomDoors() => startingRoomDoors.CloseDoors();

    [YarnCommand("kaleiodontBlink")]
    public void kaleiodontBlink() => character.StartCoroutine(character.SpriteBlinking(3f));

    [YarnCommand("kaleiodontWake")]
    public void kaleiodontWake() => StartCoroutine(kaleiodontWakeCoroutine());

    [YarnCommand("tomasFall")]
    public void TomasFall() => StartCoroutine(TomasFallCoroutine());

    public void PlayerFindsBoiler() => yarnRunner.StartDialogue(YARN_DIALOGUE_BOILER_EVENT);

    [YarnCommand("stayNearBoiler")]
    public void StayNearBoiler() => StartCoroutine(StayNearBoilerCoroutine());

    public void TomasScared()
    {
        if (tomasScared)
        {
            return;
        }
        character.DeactivatePlayer();
        tomasScared = true;
        yarnRunner.StartDialogue(YARN_DIALOGUE_TOMAS_SCARED);
    }
    public void PlayerFindsComputer()
    {
        character.DeactivatePlayer();
        yarnRunner.StartDialogue(YARN_DIALOGUE_COMPUTER_EVENT);
    }

    [YarnCommand("helenaEnter2")]
    public void HelenaEnter2() => StartCoroutine(HelenaEnter2Coroutine());

    [YarnCommand("someoneIsComing")]
    public void SomeoneIsComin() => StartCoroutine(SomeonesCominCoroutine());

    public void PlayerHidesInWardrobe()
    {
        character.DeactivatePlayer();
        character.GetComponent<SpriteRenderer>().enabled = false;
        playerHidden = true;
    }

    [YarnCommand("joeLeaves")]
    public void JoeLeaves() => StartCoroutine(JoeLeavesCoroutine());

    [YarnCommand("helenaDestroysWall")]
    public void HelenaDestroysWall() => StartCoroutine(HelenaDestroysWallCoroutine());

    [YarnCommand("playerBlinksToSayHi")]
    public void PlayerBlinksToSayHi() => character.StartCoroutine(character.SpriteBlinking(1f));

    [YarnCommand("helenaJumpsWindow")]
    public void HelenaJumpsWindow()
    {
        helena.gameObject.SetActive(false);
        escapeRoom.gameObject.SetActive(true);
    }

    [YarnCommand("footstepsSound")]
    public void FootstepsSound() => footstepsClip.Play();

    public void CompleteLevel()
    {
        GameStatus.instance.LoadNextScene();
    }

    [YarnCommand("yarnActivatePlayer")]
    public void YarnActivatePlayer() => character.Active = true;

    [YarnCommand("yarnDeactivatePlayer")]
    public void YarnDeactivatePlayer() => character.DeactivatePlayer();

    [YarnCommand("tryAgain")]
    public void TryAgain() => GameStatus.instance.ReloadScene(3);

    [YarnCommand("quitToMenu")]
    public void QuitToMenu()
    {
        GameStatus.instance.LoadMenuScene();
    }

    private IEnumerator allEnterRoomCoroutine()
    {
        john.MoveTowards(startingRoomInside2.transform.position);
        yield return new WaitForSeconds(0.3f);
        joe.MoveTowards(startingRoomInside3.transform.position);
        yield return new WaitForSeconds(0.3f);
        tomas.MoveTowards(startingRoomInside1.transform.position);
        yield return new WaitForSeconds(0.3f);
        helena.MoveTowards(startingRoomInside4.transform.position);
        StartCoroutine(WaitNPCFInishedMovingThenTurnSprite(helena, "moveY", 1f));
        StartCoroutine(WaitNPCFInishedMovingThenTurnSprite(tomas, "moveX", -1f));
        StartCoroutine(WaitNPCFInishedMovingThenTurnSprite(john, "moveX", 1f));
    }

    private IEnumerator WaitNPCFInishedMovingThenTurnSprite(NPCControllerISO npc, string parameter, float value)
    {
        yield return npc.StartCoroutine(npc.WaitForFinishedMoving());
        npc.SetAnimatorFloatValue(parameter, value);
    }

    private IEnumerator kaleiodontWakeCoroutine()
    {
        transition.TriggerFadeOut();
        yield return new WaitForSeconds(2);
        joe.gameObject.SetActive(false);
        john.gameObject.SetActive(false);
        helena.gameObject.SetActive(false);
        startingRoomDoors.CloseDoors();
        character.transform.position = startingRoomInside1.transform.position;
        character.transform.rotation = new Quaternion(0, 0, 0, 0);
        character.SetAnimatorFloatValue("moveY", -1f);
        MoveTomas();
        transition.TriggerFadeIn();
        yield return new WaitForSeconds(2);
    }

    private void MoveTomas()
    {
        tomas.transform.position = startingRoomInside5.transform.position;
        tomas.SetAnimatorFloatValue("moveY", -1f);
        tomas.SetAnimatorFloatValue("moveX", 0f);
    }

    private IEnumerator TomasFallCoroutine()
    {
        tomas.SetAnimatorFloatValue("moveY", 1);
        yield return new WaitForSeconds(1f);
        tomas.StartCoroutine(tomas.RotateCharacter(100f, 5f));
        character.Active = true;
    }

    private IEnumerator StayNearBoilerCoroutine()
    {
        boiler.gameObject.SetActive(false);
        character.DeactivatePlayer();
        transition.TriggerFadeOut();
        yield return new WaitForSeconds(2f);
        transition.TriggerFadeIn();
        character.Active = true;
        playerDry = true;
    }
    private IEnumerator HelenaEnter2Coroutine()
    {
        helena.gameObject.SetActive(true);
        helena.transform.position = helenaRoomEntrance.transform.position;
        helena.MoveTowards(helenaRoomInside.transform.position);
        yield return helena.StartCoroutine(helena.WaitForFinishedMoving());
        helena.SetAnimatorFloatValue("moveX", 0);
        helena.SetAnimatorFloatValue("moveY", 1f);
        character.SetAnimatorFloatValue("moveX", 0);
        character.SetAnimatorFloatValue("moveY", -1f);
    }

    private IEnumerator SomeonesCominCoroutine()
    {
        character.Active = true;
        hideInWardrobe.gameObject.SetActive(true);
        yield return new WaitForSeconds(5f);
        joe.gameObject.SetActive(true);
        joe.transform.position = helenaRoomEntrance.transform.position;
        joe.MoveTowards(helenaRoomInside1.transform.position);
        yield return joe.StartCoroutine(joe.WaitForFinishedMoving());
        helena.SetAnimatorFloatValue("moveY", -1f);
        if (!playerHidden)
        {
            //GameOver
            character.DeactivatePlayer();
            yarnRunner.StartDialogue(YARN_DIALOGUE_GAME_OVER);
        } else
        {
            yield return new WaitForSeconds(1f);
            yarnRunner.StartDialogue(YARN_DIALOGUE_JOE_LEAVES);
        }
    }

    private IEnumerator JoeLeavesCoroutine()
    {
        joe.MoveTowards(helenaRoomEntrance.transform.position);
        yield return joe.StartCoroutine(joe.WaitForFinishedMoving());
        helenaRoomDoors.CloseDoors();
        helenaRoomDoors.Locked = true;
        joe.gameObject.SetActive(false);
    }

    private IEnumerator HelenaDestroysWallCoroutine()
    {
        character.GetComponent<SpriteRenderer>().enabled = true;
        character.Active = true;
        helena.MoveTowards(helenaRoomInside2.transform.position);
        yield return helena.StartCoroutine(helena.WaitForFinishedMoving());
        //Move bed
        Vector3 bedEndPosition = helenaBed.transform.position + moveBedVector;

        while(Vector3.Distance(bedEndPosition, helenaBed.transform.position) > 0.01f)
        {
            helenaBed.transform.position += moveBedVector * moveBedSpeed * Time.deltaTime;
            yield return null;
        }
        //Move to wall
        escapeFloor.gameObject.SetActive(false);
        helena.MoveTowards(helenaRoomInside3.transform.position);
        yield return helena.StartCoroutine(helena.WaitForFinishedMoving());
        //Hit wall three times
        int wallHits = 3;
        for (int i = 0; i < wallHits; i++)
        {
            breakWallClip.Play();
            yield return new WaitForSeconds(0.5f);
        }
        escapeWall.gameObject.SetActive(false);
        escapeRoomFog.SetFullyTransparent();

        helena.MoveTowards(escapeRoomInside.transform.position);
        yield return helena.StartCoroutine(helena.WaitForFinishedMoving());

        helena.MoveTowards(escapeRoomInside1.transform.position);
        yield return helena.StartCoroutine(helena.WaitForFinishedMoving());
        //Open window
        openWindowClip.Play();
        escapeRoomWindow.OpenWindow();
        snowSlider.SetActive(true);
        //Timer for player
        yarnRunner.StartDialogue(YARN_DIALOGUE_ESCAPE_ROOM);

    }
}
