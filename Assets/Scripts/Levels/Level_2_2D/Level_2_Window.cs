﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_2_Window : MonoBehaviour
{
    [SerializeField] private Sprite windowClosed;
    [SerializeField] private Sprite windowOpen;

    private SpriteRenderer renderer;

    private void Awake() => renderer = GetComponent<SpriteRenderer>();

    public void OpenWindow() => renderer.sprite = windowOpen;

    public void CloseWindow() => renderer.sprite = windowClosed;
}
