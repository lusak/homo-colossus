﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogOfWar : MonoBehaviour
{
    [SerializeField] private GameObject character;
    [SerializeField] private bool discoveredAtStart = false;
    
    private SpriteRenderer spriteRenderer;
    private Color partlyTransparent = new Color(0f, 0f, 0f, .5f);
    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (discoveredAtStart)
        {
            SetPartlyTransparent();
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == character)
        {
            SetFullyTransparent();
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == character)
        {
            SetPartlyTransparent();
        }

    }

    public void SetNonTransparent()
    {
        spriteRenderer.color = Color.black;
    }

    public void SetFullyTransparent()
    {
        spriteRenderer.color = Color.clear;
    }

    public void SetPartlyTransparent()
    {
        spriteRenderer.color = partlyTransparent;
    }
}
