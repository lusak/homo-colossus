﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCController2D : MonoBehaviour
{
    [SerializeField] float moveSpeed = 30f;
    [SerializeField] float speedTimeCoefficient = 100f;
    [SerializeField] private float lastDir = 1f;

    private Animator animator;

    private bool lookForward = false;

    private void Awake() => animator = GetComponent<Animator>();

    // NPCs are moved by transform
    public IEnumerator Move(float dest, float speed)
    {
        if (lookForward) { LookForward(false); }
        animator.SetBool("moving", true);

        if (dest > transform.position.x)
        {
            if (lastDir == -1f) { FlipSprite(); }
            while (dest - transform.position.x > 0.1f)
            {
                transform.position += Vector3.right * speed * Time.deltaTime * speedTimeCoefficient;
                yield return null;
            }
            lastDir = 1f;
        }
        else
        {
            if (lastDir == 1f) { FlipSprite(); }
            while (transform.position.x - dest > 0.1f)
            {
                transform.position -= Vector3.right * speed * Time.deltaTime * speedTimeCoefficient;
                yield return null;
            }
            lastDir = -1f;
        }
        animator.SetBool("moving", false);
    }

    public void FlipSprite()
    {
        lastDir = lastDir * -1f;
        float x = transform.localScale.x;
        float y = transform.localScale.y;
        float z = transform.localScale.z;
        transform.localScale = new Vector3(-x, y, z);
    }

    public void LookForward(bool value)
    {
        lookForward = value;
        animator.SetBool("lookForward", value);
    }

    public void Kneel(bool value) => animator.SetBool("kneel", value);
}
