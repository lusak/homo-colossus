﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueContinueButton : MonoBehaviour
{
    private TextEventsManager textManager;

    private void Start() => textManager = FindObjectOfType<TextEventsManager>();

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            textManager.NextSentence();
        }
    }
}
