﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterController2D : MonoBehaviour, IPlayer
{
    private const string GROUND_COLLIDER_NAME = "GroundCollider";
    
    [SerializeField] float moveSpeed = 30f;
    [SerializeField] float dragSpeed = 10f;
    [SerializeField] float maxVelocity = 2f;
    [SerializeField] float jumpSpeed = 180f;
    [SerializeField] float teleportSpeed = 0.01f;
    [SerializeField] float speedTimeCoefficient = 100f;

    private Animator animator;
    private Rigidbody2D rBody;
    private Collider2D collider2D;

    private bool active = true;

    private Vector2 movement;
    private bool moving = false;
    private bool jump = false;
    private float lastTimeGrounded;
    private bool lookForward = false;
    private float lastDir = 1f;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        rBody = GetComponent<Rigidbody2D>();
        collider2D = GetComponent<Collider2D>();

        if (GameStatus.instance.LastCheckpoint != Vector3.zero)
        {
            transform.position = GameStatus.instance.LastCheckpoint;
        }
    }

    void Update()
    {
        if (Active)
        {
            //Move
            float x = Input.GetAxis("Horizontal");
            movement = new Vector2(x, 0);

            if (x != 0)
            {
                if (lookForward) { LookForward(false); }

                if (Mathf.Sign(x) != Mathf.Sign(lastDir))
                {
                    lastDir = x;
                    FlipSprite();
                };

                if (!moving)
                {
                    moving = true;
                    animator.SetBool("moving", true);
                }
            }
            else
            {
                if (moving)
                {
                    moving = false;
                    animator.SetBool("moving", false);
                }
            }

            if (collider2D.IsTouchingLayers(LayerMask.GetMask(GROUND_COLLIDER_NAME)))
            {
                lastTimeGrounded = Time.time;
            }

            if (Input.GetButtonDown("Jump"))
            {
                jump = true;
            }
        } else
        {
            moving = false;
            animator.SetBool("moving", false);
        }
    }

    void FixedUpdate()
    {
        if (active && (Time.time - lastTimeGrounded) < 0.1)
        {
            if (Math.Abs(rBody.velocity.x) < maxVelocity)
            {
                Move();
            }
            if (jump)
            {
                Jump();
            }
        }

        jump = false;
    }
    public void LookForward(bool value)
    {
        lookForward = value;
        animator.SetBool("lookForward", value);
    }

    public void SetPlayerActive(bool value)
    {
        if (value)
        {
            active = true;

        }
        else
        {
            active = false;
            animator.SetBool("moving", false);
        }
    }

    public IEnumerator MoveInactivePlayerByRigidBody(float destX)
    {
        if (destX > transform.position.x)
        {
            while (destX - transform.position.x > 0.1f)
            {
                if (Math.Abs(rBody.velocity.x) < maxVelocity)
                {
                    rBody.AddForce(Vector2.right * dragSpeed);
                }
                yield return null;
            }
        }
        else
        {
            while (transform.position.x - destX > 0.1f)
            {
                if (Math.Abs(rBody.velocity.x) < maxVelocity)
                {
                    rBody.AddForce(Vector2.right * dragSpeed * -1);
                }
                yield return null;
            }
        }
    }

    public IEnumerator MoveInactivePlayerByTransform(float dest, float speed)
    {
        if (lookForward) { LookForward(false); }
        if (dest > transform.position.x)
        {

            while (dest - transform.position.x > 0.1f)
            {
                transform.position += Vector3.right * speed * Time.deltaTime * speedTimeCoefficient;
                yield return null;
            }
        }
        else
        {
            while (transform.position.x - dest > 0.1f)
            {
                transform.position -= Vector3.right * speed * Time.deltaTime * speedTimeCoefficient;
                yield return null;
            }
        }
    }

    public IEnumerator SpriteBlinking(float time)
    {
        animator.SetBool("wet", true);
        yield return new WaitForSeconds(time);
        animator.SetBool("wet", false);
    }

    public IEnumerator RotateCharacter(float finalZrotation, float rotSpeed)
    {
        while (Mathf.Abs(finalZrotation - transform.localRotation.eulerAngles.z) > Mathf.Abs(rotSpeed))
        {
            transform.Rotate(0, 0, rotSpeed);
            yield return null;
        }

    }

    public IEnumerator WaitForPlayerActive()
    {
        while (!active)
        {
            yield return null;
        }
    }

    public IEnumerator WaitForPlayerActiveThenDeactivate()
    {
        while (!active)
        {
            yield return null;
        }
        active = false;
    }

    public void DontSimuleteRigidBody() => rBody.simulated = false;

    public void SimuleteRigidBody() => rBody.simulated = true;

    public bool Active
    {
        get
        {
            return active;
        }

        set
        {
            active = value;
        }
    }

    private void FlipSprite()
    {
        float x = transform.localScale.x;
        float y = transform.localScale.y;
        float z = transform.localScale.z;
        transform.localScale = new Vector3(-x, y, z);
    }
    private void Move() =>rBody.AddForce(movement * moveSpeed);

    private void Jump() => rBody.AddForce(Vector3.up * jumpSpeed);
}