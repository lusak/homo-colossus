﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportEvent : MonoBehaviour
{
    [SerializeField] private Dialogue dialogue;
    [SerializeField] private GameObject destination;
    [SerializeField] private Collider2D newCameraCollider;

    private Level_1_Narration narrator;

    private void Awake() => narrator = FindObjectOfType<Level_1_Narration>();

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            narrator.ProcessTeleport(destination.transform.position, newCameraCollider);
        }
    }
}
