﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class IntroSceneManager : MonoBehaviour
{
    [SerializeField] private TextScroller introText;

    private EarthModelManager earthModelManager;
    private PlayerMovement3D player;
    private MissionScreenController missionScreenController;
    private TutorialManager tutorialManager;
    private IntroSceneCamera camera;
    private MissionController missionController;

    void Start()
    {
        CursorManager.instance.HideCursor();
        Cursor.lockState = CursorLockMode.Confined;
        player = FindObjectOfType<PlayerMovement3D>();
        player.Active = false;
        earthModelManager = FindObjectOfType<EarthModelManager>();
        missionScreenController = FindObjectOfType<MissionScreenController>();
        tutorialManager = FindObjectOfType<TutorialManager>();
        camera = FindObjectOfType<IntroSceneCamera>();
        missionController = FindObjectOfType<MissionController>();
        missionController.onNextMission += NextMission;
        StartCoroutine(InitScene());
    }

    private IEnumerator ActivateEarthModel()
    {
        while (!(introText.FinishedScrolling))
        {
            yield return null;
        }

        yield return earthModelManager.StartCoroutine(earthModelManager.GetCloserToCamera());
    }

    private IEnumerator InitScene()
    {
        yield return StartCoroutine(ActivateEarthModel());

        missionScreenController.EnableRenderer(true);

        yield return StartCoroutine(tutorialManager.PlayTutorial());

        CursorManager.instance.SetDefaultCursor();
        CursorManager.instance.SetLockedCursor();

        player.Active = true;
    }

    private void NextMission() => StartCoroutine(LoadNextScene());

    private IEnumerator LoadNextScene()
    {
        player.Active = false;
        player.MoveToStartPosition();
        CursorManager.instance.HideCursor();
        yield return new WaitForSeconds(1f);
        missionController.ShowImage();
        yield return new WaitForSeconds(3f);

        missionController.HideImage();
        earthModelManager.StartCoroutine(earthModelManager.IntroSceneStartLanding());
        yield return earthModelManager.StartCoroutine(earthModelManager.HitStarlink());
        yield return earthModelManager.StartCoroutine(earthModelManager.EnterAtmosphere());
        yield return camera.StartCoroutine(camera.StartBurning());
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
