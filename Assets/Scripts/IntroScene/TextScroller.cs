﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextScroller : MonoBehaviour
{

    [SerializeField] private float scroolSpeed = 10f;
    [SerializeField] private float disaapearingDistance = 200f;

    private bool finishedScrolling = false;
    
    private void Start() => StartCoroutine(ScrollTextAndDestroy(transform.position));

    IEnumerator ScrollTextAndDestroy(Vector3 startPosition)
    {
        Vector3 position = transform.position;
        while(Math.Abs(startPosition.z - position.z) < disaapearingDistance)
        {
            position += transform.up * scroolSpeed * Time.deltaTime;
            transform.position = position;
            yield return new WaitForEndOfFrame();
        }
        finishedScrolling = true;
        gameObject.SetActive(false);
    }

    public bool FinishedScrolling
    {
        get
        {
            return finishedScrolling;
        }
    }
    
}
