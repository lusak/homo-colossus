﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialArrow : MonoBehaviour
{
    private Vector3 position;
    private void Start() => position = GetComponent<RectTransform>().anchoredPosition;

    public void MoveToX(int x)
    {
        Vector3 newPosition = new Vector3(x, position.y, position.z);
        GetComponent<RectTransform>().anchoredPosition = newPosition;
        position = newPosition;
    }
}
