﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour
{
    private static string EARTH_SCREEN_TEXT = "tutorial_text_planet_screen";
    private static string MATRIX_SCREEN_TEXT = "tutorial_text_matrix_screen";
    private static string TWISTER_SCREEN_TEXT = "tutorial_text_twister_screen";

    [SerializeField] private TMPro.TextMeshProUGUI textBox;
    [SerializeField] private float waitTime = 3;

    public TutorialArrow arrow;
    private LocalizationManager localizationManager;

    private void Awake() => localizationManager = FindObjectOfType<LocalizationManager>();


    public void StartTutorial() => StartCoroutine(PlayTutorial());


    public IEnumerator PlayTutorial()
    {
        yield return new WaitForSeconds(1);

        textBox.gameObject.SetActive(true);
        textBox.text = localizationManager.GetLocalizedValue(EARTH_SCREEN_TEXT);
        arrow.gameObject.SetActive(true);
        yield return new WaitForSeconds(waitTime);

        arrow.MoveToX(1);
        textBox.text = localizationManager.GetLocalizedValue(MATRIX_SCREEN_TEXT);
        yield return new WaitForSeconds(waitTime);

        arrow.MoveToX(-11);
        textBox.text = localizationManager.GetLocalizedValue(TWISTER_SCREEN_TEXT);
        yield return new WaitForSeconds(waitTime);

        textBox.gameObject.SetActive(false);
        arrow.gameObject.SetActive(false);
    }
}
