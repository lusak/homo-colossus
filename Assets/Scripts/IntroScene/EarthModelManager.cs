﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthModelManager : MonoBehaviour
{
    [SerializeField] private GameObject starlinks;
    [SerializeField] private AudioSource starlinkImpactSound;

    public bool finishedClosingIn = false;

    private IntroSceneCamera cameraController;
    
    private float finalDistanceBetweenEarthAndCamera = 80f;
    private float atmosphereThickness = 30f;
    private float closeinSpeed = 10f;

    private void Awake() => cameraController = FindObjectOfType<IntroSceneCamera>();

    public IEnumerator GetCloserToCamera()
    {
        finishedClosingIn = false;
        Vector3 newPosition = transform.position;
        while((transform.position.z - Camera.main.transform.position.z) > finalDistanceBetweenEarthAndCamera)
        {
            newPosition.z -= closeinSpeed * Time.deltaTime;
            transform.position = newPosition;
            yield return new WaitForEndOfFrame();
        }
        finishedClosingIn = true;
    }

    public IEnumerator IntroSceneStartLanding()
    {
        //Magic number
        finalDistanceBetweenEarthAndCamera = 70f;
        yield return GetCloserToCameraSlow();
        starlinks.SetActive(true);
        //Magic number
        finalDistanceBetweenEarthAndCamera = 25f;
        StartCoroutine(GetCloserToCameraSlow());
        StartCoroutine(EnlargeStarlink());
    }
    public IEnumerator EnterAtmosphere()
    {
        while ((transform.position.z - Camera.main.transform.position.z) > atmosphereThickness) {
            yield return null;
        }
    }

    public IEnumerator HitStarlink()
    {
        while (starlinks.transform.position.z > cameraController.transform.position.z + 4)
        {
            yield return null;
        }
        starlinkImpactSound.Play();
        StartCoroutine(cameraController.Shake());
    }

    private IEnumerator GetCloserToCameraSlow()
    {
        Vector3 newPosition = transform.position;
        while ((transform.position.z - Camera.main.transform.position.z) > finalDistanceBetweenEarthAndCamera)
        {
            newPosition.z -= closeinSpeed / 5 * Time.deltaTime;
            transform.position = newPosition;
            yield return null;
        }
    }

    private IEnumerator EnlargeStarlink()
    {
        while (starlinks.transform.localScale.x < 1)
        {
            Vector3 starlinkScale = starlinks.transform.localScale;
            starlinks.transform.localScale = starlinkScale + Vector3.one * 0.2f * Time.deltaTime;
            yield return null;
        }
    }
}
