﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class MissionController : MonoBehaviour
{
    [SerializeField] private TMPro.TextMeshProUGUI textBox;
    [SerializeField] private Button continueButton;
    [SerializeField] private GameObject continueButtonCollider;
    [SerializeField] private Button declineButton;
    [SerializeField] private GameObject declineButtonCollider;
    [SerializeField] private Image missionImage;


    public Dialogue dialogue;
    public event Action onNextMission;

    private LocalizationManager localizationManager;
    private Queue<string> sentences = new Queue<string>();

    private void Awake()
    {
        localizationManager = FindObjectOfType<LocalizationManager>();

        LoadNewDialogue(dialogue);
    }

    public void NextMission()
    {
        if (onNextMission == null)
        {
            return;
        } else
        {
            onNextMission();
        }
    }

    public void ProgressMission()
    {
        textBox.gameObject.SetActive(true);
        continueButton.gameObject.SetActive(true);
        declineButton.gameObject.SetActive(true);
        continueButtonCollider.SetActive(true);
        declineButtonCollider.SetActive(true);
        string key = sentences.Peek();
        textBox.text = localizationManager.GetLocalizedValue(key);
    }

    public void NextMissionInfo()
    {
        if (sentences.Count == 0)
        {
            textBox.gameObject.SetActive(false);
            continueButton.gameObject.SetActive(false);
            declineButton.gameObject.SetActive(false);
            continueButtonCollider.SetActive(false);
            declineButtonCollider.SetActive(false);
            NextMission();
        }
        else
        {
            string key = sentences.Dequeue();
            textBox.text = localizationManager.GetLocalizedValue(key);
        }
    }

    public void DeclineMission()
    {
        textBox.gameObject.SetActive(false);
        continueButton.gameObject.SetActive(false);
        declineButton.gameObject.SetActive(false);
        continueButtonCollider.SetActive(false);
        declineButtonCollider.SetActive(false);
    }

    public void LoadNewDialogue(Dialogue newDialogue)
    {
        sentences.Clear();

        foreach (string key in newDialogue.sentencesKeys)
        {
            sentences.Enqueue(key);
        }
    }

    public void ShowImage() => missionImage.gameObject.SetActive(true);
    public void HideImage() => missionImage.gameObject.SetActive(false);
}
