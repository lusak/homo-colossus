﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn;
using Yarn.Unity;

public class YarnInitializer : MonoBehaviour
{
    private DialogueRunner yarnRunner;

    private void Awake() => yarnRunner = GetComponent<DialogueRunner>();

    private void Start() => yarnRunner.textLanguage = LocalizationManager.instance.GameLanguage.LanguageShortName;

}
