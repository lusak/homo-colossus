﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thrusters : MonoBehaviour
{
    [SerializeField] private AudioSource thrustersSound;

    private SpriteRenderer renderer;
    private Animator animator;


    private void Awake()
    {
        animator = GetComponent<Animator>();
        renderer = GetComponent<SpriteRenderer>();
    }
    void Start()
    {
        renderer.enabled = false;
        animator.speed = 0f;
    }

    public IEnumerator StartThrusters()
    {
        renderer.enabled = true;
        thrustersSound.Play();
        int i = 0;
        while (i < 7)
        {
            animator.speed += 0.5f;
            i++;
            yield return new WaitForSeconds(1);
        }
    }
}
