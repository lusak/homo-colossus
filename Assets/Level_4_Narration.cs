﻿using System.Collections;
using System.Collections.Generic;
using Yarn.Unity;
using UnityEngine;

public class Level_4_Narration : MonoBehaviour
{
    private const string HELENA_DIALOGUE_FINISHED_NEW_NODE = "Level4_Dialogue1_M";

    public Dialogue endMissionDialogue;

    private PlayerMovement3D player;
    private CursorManager cursorManager;
    private Helena3D helena;
    private MissionController missionController;

    private bool finishedTalkingWithHelena;

    private void Awake() 
    {
        cursorManager = FindObjectOfType<CursorManager>();
        player = FindObjectOfType<PlayerMovement3D>();
        helena = FindObjectOfType<Helena3D>();
        missionController = FindObjectOfType<MissionController>();
    } 

    private void Start() => cursorManager.SetLockedCursor();

    [YarnCommand("endHelenaDialogue")]
    public void EndHelenaDialogue()
    {
        player.Active = true;
        cursorManager.SetLockedCursor();
        helena.helena_yarn_dialogue = HELENA_DIALOGUE_FINISHED_NEW_NODE;
        finishedTalkingWithHelena = true;
        missionController.LoadNewDialogue(endMissionDialogue);
        missionController.onNextMission += NextMission;
    }

    private void NextMission() => GameStatus.instance.LoadNextScene();
}
