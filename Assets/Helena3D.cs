﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

public class Helena3D : MonoBehaviour
{
    [SerializeField] public string helena_yarn_dialogue;

    private DialogueRunner yarnRunner;
    private PlayerMovement3D player;
    private CursorManager cursorManager;

    private void Awake() {
        cursorManager = FindObjectOfType<CursorManager>();
        player = FindObjectOfType<PlayerMovement3D>();
        yarnRunner = FindObjectOfType<DialogueRunner>();
    }
    private void OnMouseDown() {
        player.Active = false;
        cursorManager.SetMovableCursor();
        yarnRunner.StartDialogue(helena_yarn_dialogue);
    } 
}
