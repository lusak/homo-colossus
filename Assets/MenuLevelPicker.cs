﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuLevelPicker : MonoBehaviour
{
    public void ChooseLevel(int levelNumber) => SceneManager.LoadScene(levelNumber);
}
