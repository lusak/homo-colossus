Homo Colossus - my first game in Unity. We play as an alien, whose task is to supervise the planets in the part of the Milky Way. The alien visits Earth after five thousand years and is surprised by the changes that have taken place here. I treated the game as a chance for learning of as many mechanics as possible.  It has 3D, 2D left to right, 2D top-down, 2D Isometric levels. Game is unfinished. 

Genre: Adventure

Features / solutions:

- 2D, 2D Isometric, 3D, game focused on the story, animated scenes, localization (polish & anglish languafes to choose)
-  dialogue system created with Yarn Spinner tool
- pathfinding system created with A * Pathfinding project
- platform - Windows

Link to YT video in which I present the game (video in polish):
https://www.youtube.com/watch?v=hZWsUSt4Xd0

To play the game pull the repository, add project to Unity and build it. It should work (It works for me :)

Unity version: 2019.3.10f1